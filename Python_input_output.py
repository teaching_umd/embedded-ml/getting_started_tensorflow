# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 12:17:16 2021

@author: sshah389
"""


import serial
import numpy as np
from matplotlib import pyplot 
import math
i=0
ser = serial.Serial('COM8', 9600, timeout=1) ##change this to your COMport

SAMPLES = 1000

x_values = np.random.uniform(low=0, high=255/100, size=SAMPLES)
y_values = np.sin(x_values)

y_pred=np.zeros([SAMPLES])
while i<SAMPLES:
    int_value=int(x_values[i]*100)

    ser.write(int_value.to_bytes(1,'big'))
 
    out_byte =  ser.readline()
    out_string = out_byte.decode()
    out_string1 = out_string.strip()
    out_int = float(out_string1)
    y_pred[i]=out_int
    i=i+1


pyplot.plot(x_values,y_pred,'r*')   
pyplot.plot(x_values,y_values,'b.')    
ser.close()
